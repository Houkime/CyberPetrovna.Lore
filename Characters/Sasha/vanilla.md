## Full Name:
Alexandra Victorovna Futyagina

## Bio:
* born in the Capital in 2005-ish
* parents:
  * Victor (metro train pilot)
  * Mila (kindergarten teacher)
* parents very conservative
* parents watch a lot of TV
* Sasha is dissatisfied with her life with parents
* Sasha's Internet is severely restricted both time-wise and content-wise
* Sasha's school is mediocre at best, and degrades fast with political situation
* Sasha is dissatisfied with her classmates
* Recently there is too much political and clerical propaganda in the school
* She asked uncomfortable questions and was intimidated for this by teachers
* She didn't know about her talents because nobody was really invested in finding them
* She found Engineer herself.
* She enjoys freedom and infrastructure around him (in contrast with her other life)
* She pursues her own projects (she isn't his assistant though sometimes helps with interesting stuff)

## Stats & perks:

* She is curious enough to find things on her own
* She's rather self-centered
* She is not submissive.
* She can feel lies
* She can lie.
* She can manipulate, but it is not a self-goal.
* She can learn and analyze things fast
* She doesn't know much beyond standard program though cause her sources were
artificially limited by parents.
* Her life goals are yet undecided.
* Moral-wise she is cloudy because she was surrounded by doublethinkers.
* Logical inconsistencies bug her.
* She retains some intuitive moral compass (more or less standard pro-social) until she can come up with her own replacement.
* She is not a doublethinker herself yet (and will probably never be because of
  the Engineer)
