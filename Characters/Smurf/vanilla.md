## Full Name

THE Smurf

## Nickname

Smurf

## Type

Dog

## Sex

Male

## Appearance

* black
* short fur
* yellow eyebrows
* cyberleg (back right)
* Eyepiece similar to Petrovna's

## Bio

* Unknown birth date
* Found by Sasha lying on the street in winter
	* Leg frostbitten
	* left eye burned out by a sigarette
	* starving
* Operated and augmented by Engineer
* Now lives in his basement
* Sasha's parents won't let him in easily
* When Engineer goes out of city Sasha goes to take the dog