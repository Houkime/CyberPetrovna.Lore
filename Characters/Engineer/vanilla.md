## Full Name:
Petr Nikolaevich Kuchnov

## Nickname:
BoggleBurner (or BB)

## Bio:

* Born in the Capital in 1992
* Medium welfare
* Early computer usage
* Ignorant parents
  * After a hot self-defence he was pretty much on his own
* Called genius in school
* Grew depressive and suicidal
* After a suicide failure decided his core problem - BOREDOM
  * who cares what happens next if boredom will kill him anyway
* Quitted official education in favor of his own goals
* Since then self-edu-ed to hell and back


## Stats & perks:

 * Extreme Funlust
 * CANT do things he doesn't consider fun
 * Any boredom depresses him to the point of suicidal thoughts
 * Technologically omnipotent: given some time for edu, trial and error can do literally _anything_ that is not forbidden by physics
 * He KNOWS about his omnipotency
 * Almost totally amoral
  * considers killing people too primitive to be fun though - creating new stuff is more interesting
  * Sasha holds him from doing the most extremely questionable stuff... as she can (probably she's the only reason country still holds)
 * Hates government because it is boring and tries to enforce boredom among people including him (which will kill him)
 * Generalistic Knowledge:
  * Knows the basics of nearly every major science and works with the net and friends to complement for gaps
 * Not really interested in money
