## Tribal

* Most aggressive clans win
* Rise of domination cult

## BDSM Empire

* slavery of 99% of population
* hierarchical domination and graded slaves
* violence and rape determine hierarchy (-> large homosexual activity)
* cult of pleasure among elites
* hierarchy is seen more important then economical success of a country
* massive economically negative geographical expansion to dominate over more people

## Hype Union

* advent of Hype methods of psychological influence
They complement physical and sexual domination.
* Slavery is formally no more but violent hierarchy de-facto remains
* Adepts of Hype over-dominate previous elites who stick to old domination methods
* Church of God considered rival hype and eliminated
* Sex is tabooed to decrease it's non-domination usage
* Lies and absurd levels lead to mass psychosis and doublethink
* Mass psychosis kills remaining 25% of healthy population
* HypeEaters are born to provide easy-to-use infrastructure for mass killings during this period. You just need to write a letter with the name of an "enemy of the Hype" to them
* Government itself is affected by psychosis it induced
* Failure to join Hype Reich (because of government greed)
* Hysterical War with Hype Reich
When it seemed when psychosis can't be even greater, war increases it twofold giving birth to Hysteria
  * Mass conscription through hype and force
  * Additional 50% of population dies in war because of hysterical commands and hysterical heroism
  * war ends with losses 10:1 between Union and Reich (the latter was less hysterical and more advanced)
  * Union wins nevertheless because of its sheer size
* Auxiliary post-traumatic cult of Hysterical Hype War
  * Any critics of war is forbidden
  * National holiday established
* Economical troubles
* Hype becomes weaker because of economy
* Hype Union is killed by economy

## Hype Federation

* pseudo-democracy + market on top of old domination hierarchy
* domination hierarchy is used to subvert market
* Cleptocracy is born
* Church of God is restored
* Bald President is elected
* Bald President makes himself a new Bald Hype
* Hype of God and Bald Hype merge with the cult of HHW
* HypeEaters numbers are enlarged manyfolds
