* Sasha was dissatisfied with her life with parents.
* Sasha was dissatisfied with her classmates.
* Sasha's Internet is severely restricted both time-wise and content-wise.
* Her parents are loyalists and watch a lot of TV.
* For her life was a bit more frightening than for the Engineer.
* She didn't know about her talents because nobody was really invested in finding them.
* But she was curious enough to find things on her own.
* She's rather self-centered.

## First Encounter

* She found the Engineer's hideout occasionally in early morning.
* He was busy testing his spider-robot on the playground when nobody was around.
* Spider-robot didn't do really well. It had a hard time with the stairs.
* Engineer noticed she's looking but did nothing since children weren't ones to hide his activity from.
(In the normal country he wouldn't even need to hide... for the most part)
-Did you make it yourself?
-Yep.
*pause*
-It is not very good.
-Umgh... well, it will be eventually.
-No wonder I don't see them around.
*pause. Engineer starts to make stupid mistakes*
-You don't seem very smart. Does it mean I can make one too?
-Hhhhh... grhm. It depends. Do you like physics?
-Not really. But evil scientists are cool and don't fear anything.
They also somehow happen to not have parents and be very rich. Would be nice to be one.
-Nah, you don't need to be a scientist for that. This country is run by plain villains without any degree.
-You think the Bald President is a villain? TV says he's good. And helping birds.
A bit boring though. Always says the same things with the same face.
-Do you even use internet?
-Parents disallow me to use it for long. 
Say it is bad for study and also i'm too young.
Though they're watching TV for much longer despite they have a work to do.
-And you don't even have a phone?
-I have, but it is kinda old. *shows the phone*
-Oh man... No wonder you're here in 5 am.
Consider making yourself a phone instead of a robot.
-Is it harder?
-Well, I somehow made. *shows her*
-Ewww... bulky.
-Maybe. But it is more able than a usual one.
-How's that?.
-It is easier to program, it has a free OS, it also has extention slots to plug whatever you want into it.
Also it doesn't spy on you which is a must here.
*Engineer suddenly understands that he's talking to a child*
*He stops.*
-And what about camera?? How many megapixels?
-Uh? Not really much. WalledGardenPhone has more.
-Hmmm... But it will be free for me if I make it, right?
-Ahhh... If you really want. I have some spare parts left. Probably. Can't guarantee it will work though.
-When can I make it?

_____________________