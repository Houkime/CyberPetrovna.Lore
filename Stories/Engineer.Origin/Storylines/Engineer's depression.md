* [ASSERT: Engineer.Ignorant-parents] Ignorant parents
  * Parents WERE worrying about him, but it was more about their
  psychological complexes and thin-veiled desire of control
  * After a hot self-defence he was pretty much on his own
  * His door received a beating of them trying to force their way in.
* Called genius in school
* Grew depressive and suicidal
* After a suicide failure decided his core problem - BOREDOM
  * who cares what happens next if boredom will kill him anyway
* Quitted official education in favor of his own goals
* Since then self-edu-ed to hell and back
