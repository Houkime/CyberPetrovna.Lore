 * Fun vs Dullness
 * Fun vs Fear
 * "Live for fun" vs "Live to survive"
 * Liberty vs Control
 * Generalism vs Specialism
 * Individual moral vs social moral
 * Acting vs Inert
 * Future vs Past
 * Strive for Change vs Fear of Unknown
 * Change vs Stagnation
 * Technical methods vs Political methods of change
 * Control by society vs dictatorship
