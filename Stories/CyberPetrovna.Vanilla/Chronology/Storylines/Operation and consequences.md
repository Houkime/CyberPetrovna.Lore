### Operation actions (Scene "operation")

* Everything was filmed and uploaded to Peertube with NSFW mark
* No telemetry was installed
* Remote consultations and viewers
* One of the eyes with cataract was replaced with neuroconnected camera piece (+microcomputer with Linux and Wifi)
* Faulty heart was replaced with a pump
* Spine reinforcement (not very invasive. Provides usb ports for debugging)
* Hormonal regulators (not enough to make her fertile though?)
* Senolysis (for better compatibility)
* Mini fission reactor to power pump and electronics
* Mechanical dildo (fully actuated and sensorized)

### Consequences:

* No subway for Petrovna anymore. Too much metal.
* No airports or trains either.
* Although she doesn't look TOO suspicious, she has an eye piece and 
her walking gait is a bit odd. Police might want to ask her questions.
She COULD say it's an AR headset though (which would be a partial true)
... if she knew what AR is, that is...
* [Longterm, not immediate] - plutonium dependency.
* Immediate internet access.
This would be huge... If again she was interested in it.
* [Longterm, delayed] Heightened cancer rate from both radiation and senolysis.
* [MAJOR] Popularity
  * Her face is now known in technical circles.
  * Tech people are suddenly friendly with her and willing to help.
    * Worth noticing at first they are not really interested in her as a human with merits though.
  * Though does she want it?
  
  

### Questions that arise

* [MAJOR EXISTENTIAL] She has now much more years to live and many sources of struggling 
were deleted. What she will do? What are her real interests?
* She better keep in contact with the Engineer in case sth bad happens,
but will she?
* The world as she knows it is no longer there.
It's a bit like time travel... just you don't really move anywhere.
It's just when you suddenly recognize that things are gone and new
are unfamiliar and frightening.
And it got to the point when new things literally invaded her to allow her to live,
so there not a slightest chance to escape facing them.
[SHE CANT LIVE LIKE SHE USED TO ANYMORE.
THIS IS THE POINT OF REEVALUATION AND CHOICE FOR PETROVNA.]
(and it is personal in the first place and not political.)
If the life was suddenly new and she was totally helpless that's the one thing
BUT SHE WAS EXPLICITLY GIVEN ADDITIONAL CAPABILITIES TO COPE WITH IT.
It's just the question: will she acknowledge and master new capabilities?
And if she can do it, maybe there were no real problems with her previous life?
And she just wasted it for nothing?
* Where she will go? She has no worthy relatives and almost no friends?
