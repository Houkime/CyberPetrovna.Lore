# Operation

### Place
Engineer's basement

### Time 

TBD

### Actors

* Petrovna (Subject)
* Engineer and his robotics. (implementor)
* Sasha (Casual bystander)
* Teleoperation robot with an expert surgeon connected.

### Purpose

* Cyborgization techniques testing. (VERY IMPROPER)
* Manual documenting.

### things done

* Everything was filmed and uploaded to Peertube with NSFW mark
* No telemetry was installed
* Remote consultations and viewers
* One of the eyes with cataract was replaced with neuroconnected camera piece (+microcomputer with Linux and Wifi)
* Faulty heart was replaced with a pump
* Spine reinforcement (not very invasive. Provides usb ports for debugging)
* Hormonal regulators (not enough to make her fertile though?)
* Senolysis (for better compatibility)
* Mini fission reactor to power pump and electronics
* Mechanical dildo (fully actuated and sensorized)

(It was possible to go further with alterations but for test purposes
invasion better be minimal)

### Actors actions

* Engineer performs the operation and talks to Sasha and viewers.
  * He doesn't actually hold a scalpel, but rather fiddles with scripts
  of DaVinci-like many-handed robot.
  * Sits beside his computer.
  * He feels:
    * more or less like usual.
    * though it is more serious stuff that he usually handles
    * In his eyes it is more or less the same as repair+modding. Same principles apply.
    * A bit amazed how well things go so far.
    (Although he prepared for this quite a bit, of course petrovna's finding 
    was a bit untimely)
* Also there is one robot teleoperated by a friend surgeon who helps him.
  * He is quite concentrated and excited at the same time.
  * He feels mixed:
    * On one hand he knows they are essentially saving this woman.
    * And that Engineer with perform the operation with or without him.
    * And he really doubts Engineer's methods' provess in surgical field.
    * On the other hand they way they save her may not be totally considered humane.
    let alone voluntary, let alone legal.
  * And yes, when Engineer suddenly asked him to help he was in the middle of something.
  (something quite silly but important for him nonetheless)
  * Feels especially strange during module tests including organ testing. 
  (it is very unusual for a doctor to witness such things performed on a human)
* Sasha: 
    * feels strange but also quite interested. First time she 
    * Wants dildo for herself also.
    * Engineers says he can build her a strapon if she wants.
    * Viewers say he has a bad influence on children and recommend Sasha to run.
    * Doesn't completely understand English and uses Russian when she is out of words
    (viewers: HOW CUTE!)
  
