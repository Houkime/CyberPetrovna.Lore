# CyberPetrovna.Lore

The repository for [collective] lore development of CyberPetrovna universe and characters.
Unofficial main music theme - S.S.H (Saitama Saishyuu Heiki): Detroit Marisa City

## Intro

CyberPetrovna is a story framework/constructor set in grostesque Hype Federation (derived
from IRL Putin's Russia) with the vanilla story revolving around an elderly woman Petrovna
who was driven by anticonstitutional laws and state mafia to the poverty and the brink of death.
To be (involuntarily) saved and "repaired" by outlawed volunteer OSHW cyborgization dev.
To presumably kick some butt, although not really in a Hollywood-style bruteforce manner.
The outcome of vanilla plot is still undecided (feel free to fork and open issues).

## Repo structure
Might be a bit unconventional. This is a first (clumsy) try on a collaborative git-based
lore writing system.
It is tiered more or less untuitively if you consider it a "software" project with
high-level specifications, lower-level specifications and finally build stages.
With characters and setting being modules existing independent from any particular storyline.

Unfortunately build (for now) cannot be automated until we have some fancy neural networks
So build stages are also an object of collaboration and is under SVC control which is not otherwise
advisable.

It's possible to migrate to interesting FOSS tools like node-based Leo Editor in the future
or write own FOSS software for the cause once the format is semi-established and first proto-product is done.