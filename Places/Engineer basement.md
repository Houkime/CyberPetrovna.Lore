## Exterior:

* Is located in the outskirts of the Capital
* Homes in the district are mostly quite old and mostly yellow 5-stories
* Very often there's a smell of pelmeni dumplings being cooked (may be some interesting neighbor char)
* A railroad and a forest are nearby and sometimes noisy

## Interior:

* Very crammed although quite large (one of the reasons Engineer wants to move out of city)
* Filled with tech:
	* sth resembling a DIY mainframe made from sigleboards.
	* Lots of discarded stuff being kept as part donors including an old TV and a fridge.
	* Multiple notebooks, some disassembled, kept as a stack
	* Testing rigs for robotics
	* Remote presence + general assistance bot
	* Printer + recycler (with a manipulator hanging near it)
* Working table with medical/fabrication bot above it
* Computer table
* Tools on the walls
* foldable bed near computer

## Inhabitants

* Smurf (Dog)
* Engineer
* Sasha